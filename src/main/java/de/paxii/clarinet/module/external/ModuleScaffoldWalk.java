package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.player.PlayerUpdateWalkingEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.CPacketPlayer;
import net.minecraft.network.play.client.CPacketPlayerTryUseItemOnBlock;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;

public class ModuleScaffoldWalk extends Module {

  public ModuleScaffoldWalk() {
    super("ScaffoldWalk", ModuleCategory.MOVEMENT);

    this.setRegistered(true);
    this.setDescription("Places a bridge when you walk off of an edge");
    this.setVersion("1.0.1");
    this.setBuildVersion(18201);
  }

  @EventHandler
  public void onPlayerMovement(PlayerUpdateWalkingEvent playerUpdateWalkingEvent) {
    EntityPlayerSP thePlayer = Wrapper.getPlayer();
    boolean placeBridge = thePlayer.onGround
            && Wrapper.getWorld().getCollisionBoxes(
            thePlayer,
            thePlayer.getEntityBoundingBox().offset(0, -0.5, 0).expand(-0.25, 0, -0.25)
    ).isEmpty();

    ItemStack itemStack = thePlayer.getHeldItem(EnumHand.MAIN_HAND);
    if (placeBridge && Item.getIdFromItem(itemStack.getItem()) != 0 && itemStack.getItem() instanceof ItemBlock) {
      EnumFacing horizontalFacing = thePlayer.getHorizontalFacing();
      EnumFacing opposite = horizontalFacing.getOpposite();

      BlockPos blockPos = new BlockPos(
              thePlayer.posX, thePlayer.getEntityBoundingBox().minY, thePlayer.posZ
      ).down(!thePlayer.onGround ? 2 : 1).offset(opposite, 1);

      float facingX = 0.5F;
      float facingY = 0.5F;
      float facingZ = 0.5F;

      switch (opposite.getAxis().getName()) {
        case "x":
          facingX = horizontalFacing.getFrontOffsetX() == 1 ? 1.0F : 0.0F;
          break;
        case "z":
          facingZ = horizontalFacing.getFrontOffsetZ() == 1 ? 1.0F : 0.0F;
          break;
      }

      float[] angles = this.getAngles(blockPos);
      Wrapper.getSendQueue().addToSendQueue(new CPacketPlayer.Rotation(angles[0], angles[1], thePlayer.onGround));
      Wrapper.getSendQueue().addToSendQueue(new CPacketPlayerTryUseItemOnBlock(
              blockPos, horizontalFacing, EnumHand.MAIN_HAND, facingX, facingY, facingZ
      ));
      thePlayer.swingArm(EnumHand.MAIN_HAND);
    }
  }

  private float[] getAngles(final BlockPos blockPos) {
    double difX = (blockPos.getX() + 0.5D) - Wrapper.getPlayer().posX,
            difY = (blockPos.getY() + 0.5D)
                    - (Wrapper.getPlayer().posY + Wrapper.getPlayer().getEyeHeight()),
            difZ = (blockPos.getZ() + 0.5D) - Wrapper.getPlayer().posZ;
    double helper = Math.sqrt(difX * difX + difZ * difZ);
    float yaw = (float) (Math.atan2(difZ, difX) * 180 / Math.PI) - 90;
    float pitch = (float) -(Math.atan2(difY, helper) * 180 / Math.PI);
    return (new float[]{yaw, pitch});
  }

}
